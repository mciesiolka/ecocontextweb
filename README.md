![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

# Goal of the project
The main goal of this project is to provide a better context for translation of
[Eco game](https://strangeloopgames.com/eco).

# How to colaborate
## Taking screenshots

* In-game screenshots: use Steam (press F11).
* Web client:
  * Enter proper page
  * Press F11 to enter full screen mode
  * Press Windows + PrnScr to capture image which will be stored in My Documents\Pictures\Screenshots

## Preparing json files
The structure of the file is as follow:
* `id` - ID according to Crowdin.com (i.e. `36400`):

![How to find the ID](https://mciesiolka.gitlab.io/ecocontextweb/HowToFindID.png)

* `name` - A full original (english) string to translate.
* `description` - a short description of a context of the string.
    Must exist but may be empty.
* `steps` - an array of strings containing steps necessary in order to see
    the string to translate.
* `figures` - an array of objects of the following structure:
    * `path` - a path relative to root directory (i.e. `"img/web_loading.png"`)
    * `coords` - a coordinates of box marking a place of the image where the
      string in subject is visible. The value is a string of four comma separated
      numbers (`"ux,uy,lx,ly"`) which are coordinates of upper left and lower right corder of the
      rectangle. The coordinates are pixel distance from upper right corner of
      the image. Best way to find the values is to use Paint and hover over
      points. Thanks to this parameter you may reuse the same screenshot to
      illustrate multiple strings.

![How to find coordinates](https://mciesiolka.gitlab.io/ecocontextweb/HowToFindCoordinates.png)
