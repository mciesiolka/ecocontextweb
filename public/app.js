var hash = window.location.hash.substr(1);  
var gitUrl = '//mciesiolka.gitlab.io/ecocontextweb/';

var strokeWidth = 7;

var updateData = function(id) {
  console.log("Function called!");
  const xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState === 4 && this.status === 200) {
      console.log("Received data!");
      console.log(this);
      setDataFromJson(this);
    } else {
      console.log("Error!");
      console.log(this);
    }
  };
  xhttp.open("GET", this.gitUrl + "data/" + id + ".json", true);
  xhttp.send();
};

var setTitle = function(id) {
  document.title = "Eco Translation Context Provider - " + id;
};

var setHeader = function(content) {
  document.getElementById("header-text").innerHTML = content;
};

var clearImages = function() {
  let images = document.getElementById("figures");
  while(images.lastChild) {
    images.removeChild(images.lastChild);
  }
};

var generateCanvasContext = function(node, array) {
  const ctx = node.getContext("2d");

  const image = new Image();
  image.onload = drawImageActualSize.bind(this, image, ctx, array[1]);
  image.src = gitUrl + array[0];

};

var translateCoords = function(coords) {
  let newCoords = coords.split(",");
  newCoords[0] -= strokeWidth;
  newCoords[1] -= strokeWidth;
  newCoords[2] -= newCoords[0] - strokeWidth;
  newCoords[3] -= newCoords[1] - strokeWidth;

  return newCoords;
};

var drawImageActualSize = function(image, ctx, coords) {
  ctx.width = image.naturalWidth;
  ctx.height = image.naturalHeight;
  ctx.canvas.width = image.naturalWidth;
  ctx.canvas.height = image.naturalHeight;
  ctx.drawImage(image, 0, 0);

  coords.forEach(function(coord){
    drawRectangle(ctx, coord);
  });
};

var drawRectangle = function(ctx, coords) {
  ctx.beginPath();
  ctx.rect.apply(ctx, translateCoords(coords));
  ctx.lineWidth = strokeWidth;
  ctx.strokeStyle = "red";
  ctx.stroke();
  ctx.closePath();
};

var addImages = function(imagesArray) {
  let imagesRoot = document.getElementById("figures");
  imagesArray = Object.entries(imagesArray);

  imagesArray.forEach(function(entry){
    let node = document.createElement('canvas');
    generateCanvasContext(node, entry);
    imagesRoot.appendChild(node);
  });
};

var transformImagesArray = function(array) {
  let newArr = {};
  array.forEach(function(el){
    if (newArr.hasOwnProperty(el.path)) {
      newArr[el.path].push(el.coords);
    } else {
      newArr[el.path] = [el.coords];
    }
  });
  return newArr;
};

var clearSteps = function() {
  let list = document.getElementById("steps");
  while(list.lastChild) {
    list.removeChild(list.lastChild);
  }
};

var addSteps = function(stepsArr) {
  if (!Array.isArray(stepsArr)) {
    stepsArr = [stepsArr];
  }
  let list = document.getElementById("steps");
  stepsArr.forEach(function(el){
    var li = document.createElement('li');
    li.innerHTML = el;
    list.appendChild(li);
  }) ;
};

var setDescription = function(content) {
  document.getElementById("description").innerHTML = content
};

var setDataFromJson = function(content) {
  let response = JSON.parse(content.response);
  setTitle(response.id);
  clearSteps();
  addSteps(response.steps);
  setHeader("#" + response.id + ": " + response.name);
  setDescription(response.description);
  clearImages();
  addImages(transformImagesArray(response.figures));
};

var listenForHashChange = function() {
  window.addEventListener("hashchange", function(event){
    hash = window.location.hash.substr(1);
    updateData(hash);
  });
};

listenForHashChange();
if (hash != '') {
  updateData(hash);
}